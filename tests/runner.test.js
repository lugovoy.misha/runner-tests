const {asyncRunner} = require('../index');

const sum = (a, b) => a+b;
const pow = (a, b) => a**b;
const mul = (a, b) => a*b;
const diff = (a, b) => a-b;
const applyAsync = (fn, t = 0) => (...args) => setTimeout(args.pop(), t, fn(...args));


describe('tests for runner', () => {

  test('should be end the same time as th biggest time from function of asyncFn array has', (done) => {
    let asyncFnc = [
      applyAsync(diff),
      applyAsync(sum, 100),
      applyAsync(mul, 1000),
      applyAsync(pow, 2000),
    ];

    let date = Date.now();
    global.console.log = (result) => {
      const dateEnd = Date.now() - date;
      expect(dateEnd < 2100).toBeTruthy();
      done();
    }
    asyncRunner(3, 3)(asyncFnc);
  });

  test('result of each function of asyncFn array should have the same index in the result array - 1', (done) => {
    let asyncFnc = [
      applyAsync(diff),
      applyAsync(sum, 100),
      applyAsync(mul, 1000),
      applyAsync(pow, 2000),
    ];

    global.console.log = (result) => {
      expect(result[0]).toBe(0);
      expect(result[1]).toBe(6);
      expect(result[2]).toBe(9);
      expect(result[3]).toBe(27);

      done();
    }

    asyncRunner(3, 3)(asyncFnc);

  });

  test('result of each function of asyncFn array should have the same index in the result array - 2 ', (done) => {
   let  asyncFnc = [
      applyAsync(diff, 2000),
      applyAsync(sum, 1000),
      applyAsync(mul, 100),
      applyAsync(pow),
    ];
    global.console.log = (result) => {
      expect(result[0]).toBe(0);
      expect(result[1]).toBe(6);
      expect(result[2]).toBe(9);
      expect(result[3]).toBe(27);

      done();
    }
    asyncRunner(3, 3)(asyncFnc);

  });
  test('result of each function of asyncFn array should have the same index in the result array - 3', (done) => {
    let asyncFnc = [
      applyAsync(diff, 200),
      applyAsync(sum, 1000),
      applyAsync(mul, 200),
      applyAsync(pow),
    ];
    global.console.log = (result) => {
      expect(result[0]).toBe(0);
      expect(result[1]).toBe(6);
      expect(result[2]).toBe(9);
      expect(result[3]).toBe(27);
      done();
    }
    asyncRunner(3, 3)(asyncFnc);
  });
  test('should have  the same length', (done) => {
    let asyncFnc = [
      applyAsync(diff),
      applyAsync(sum, 100),
      applyAsync(mul, 1000),
      applyAsync(pow, 2000),
    ];
    global.console.log = (result) => {
      expect(result.length).toBe(4);
      done();
    }
    asyncRunner(3, 3)(asyncFnc);
  });
});
